from fastapi import FastAPI

from .translation import MONOSPACE

app = FastAPI(title="Transunico")


@app.get("/mono")
def monospace(q: str):
    return {"value": q.translate(MONOSPACE)}


@app.get("/")
def index():
    return {}
