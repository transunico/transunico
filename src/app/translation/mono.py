_caps_a = ord("A")
_caps_a_mono = ord("\N{mathematical monospace capital a}")

_smol_a = ord("a")
_smol_a_mono = ord("\N{mathematical monospace small a}")

_digits = ord("0")
_digits_mono = ord("\N{mathematical monospace digit zero}")

MONOSPACE = {ord(" "): "\N{en space}"}

MONOSPACE.update(
    zip(range(_caps_a, _caps_a + 26), map(chr, range(_caps_a_mono, _caps_a_mono + 26)))
)

MONOSPACE.update(
    zip(range(_smol_a, _smol_a + 26), map(chr, range(_smol_a_mono, _smol_a_mono + 26)))
)

MONOSPACE.update(
    zip(range(_digits, _digits + 10), map(chr, range(_digits_mono, _digits_mono + 10)))
)
