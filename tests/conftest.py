import pytest
from fastapi.testclient import TestClient


@pytest.fixture
def app():
    from app.main import app

    return app


@pytest.fixture
def test_client(app):
    return TestClient(app)
