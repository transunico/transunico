from starlette.testclient import TestClient


def test_title_is_transunico(app):
    assert app.title == 'Transunico'


def test_index(test_client: TestClient):
    assert test_client.get('/').json() == {}


def test_mono_translate(test_client: TestClient):
    assert len(test_client.get('/mono', params={'q': 'test?'}).json()['value']) == 5
